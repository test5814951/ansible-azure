# Ansible Project: VM Configuration and Terraform Deployment

## Overview

This Ansible project automates the configuration of virtual machines, including software updates, Git and Terraform installations, cloning a Git repository, initializing Terraform, creating a Terraform plan, applying the plan to deploy infrastructure, and storing the Terraform state file in Azure Storage.

## Prerequisites

Before running the Ansible playbook, ensure you have the following prerequisites installed:

- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
- [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
- [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)

## Configuration

1. **Azure Credentials:**

   Make sure you are logged into Azure using the Azure CLI:

   ```bash
   az login
   ```

**Ansible Inventory:**

Update the inventories/your_inventory_file with your VM details:

```bash
[your_vm_group]
your_vm_host ansible_host=your_vm_ip
```

**Variable Configuration:**

Update the variable values in the vars/vars.yml file:
```bash
---
your_vm_group: your_vm_group
your_vm_ip: your_vm_ip

your_non_root_user: your_non_root_user

your_username: your_username
your_repository: your_repository

your_resource_group: your_resource_group
your_storage_account: your_storage_account
your_container: your_container
your_state_file: your_state_file.tfstate
your_sas_token: your_sas_token
terraform_env: your_envname
```
## Usage
Run the Ansible playbook with the following command:

```bash
ansible-playbook -i inventories/your_inventory_file playbooks/setup_vm_combined.yml
```
## Individual Stages

You can run individual stages using tags. For example, to install Terraform:

```bash
ansible-playbook -i inventories/your_inventory_file playbooks/setup_vm_combined.yml --tags=install_terraform
```

**Available tags:**

update_packages

install_packages

add_gpg_key

add_apt_repository

install_terraform

clone_git_repo

init_terraform

create_terraform_plan

apply_terraform_plan (includes terraform apply)

upload_terraform_state

## Run Specific Stages
If you want to run only specific stages without running the entire playbook, you can use combinations of tags. For example, to run Terraform initialization and apply only:

```bash
ansible-playbook -i inventories/your_inventory_file playbooks/setup_vm_combined.yml --tags=init_terraform,apply_terraform_plan
```
Adjust the tags according to the specific stages you want to execute.

## Additional Notes

- This project assumes the use of an Azure Storage account for Terraform state. Adjust the storage configuration based on your chosen backend.
- Always test playbooks in a controlled environment before applying changes to production systems.
